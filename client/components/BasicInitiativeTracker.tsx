import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import * as FieldNames from './Common/FieldNameLablePlaceHolder';
import InitiativeTrackerInput from './Common/InitiativeTrackerInput';


const endTurnStyle = {
    marginLeft: "0.5em",
}

const startCombatBtn = {
    marginTop: "2em",
    marginBottom: "1em"
}

export default class BasicInitiativeTracker extends React.Component<RouteComponentProps<{}>, IInitiativeTracker> {
    constructor(props: any) {
        super(props)

        this.state = {
            showInitiative: false,
            count: 0,
            round: 0,
            monsterName: "",
            initiativeTracker: [{
                name: "",
                initiative: 0,
                isYellow: true,
                isGreen: false,
                isDisabled: false
            }],

        };
    }

    private handleSubmit(e: React.FormEvent<HTMLFormElement>): void {
        e.preventDefault();
    }

    handleAdd() {
        this.setState({
            count: this.state.count + 1,
            initiativeTracker: this.state.initiativeTracker.concat([{
                name: "",
                initiative: 0,
                isYellow: true,
                isGreen: false,
                isDisabled: false
            }]),

        })

    }

    private handleChangeName(e, index) {
        const newName = this.state.initiativeTracker.map((tracker, sidx) => {
            if (index !== sidx) return tracker;
            return { ...tracker, name: e.target.value }
        })

        this.setState({ initiativeTracker: newName })
    }

    private handleChangeInitiative(e, index) {
        const newInitiative = this.state.initiativeTracker.map((tracker, sindex) => {
            if (index !== sindex) return tracker;
            return { ...tracker, initiative: e.target.value }
        })

        this.setState({ initiativeTracker: newInitiative })

    }

    handleRemove(index) {
        this.setState({
            initiativeTracker: this.state.initiativeTracker.filter((s, sindex) => index !== sindex)
        });
    }

    decideColor(tracker): string {

        if (tracker.isYellow) {
            return "btn-warning"
        }
        else if (tracker.isGreen) {
            return "btn-success"
        }
        else if (tracker.isDisabled) {
            return "btn-info"
        }
        return "";
    }
    show() {

        this.state.initiativeTracker.sort((obj1, obj2) => {
            return obj2.initiative - obj1.initiative;
        })

        return <div >
            <button
                className="btn btn-primary"
                style={startCombatBtn}
                onClick={() => (
                    this.setNextElement(-1),
                    this.setState({ round: 1 }))}
            >Start Combat</button>

            {this.state.initiativeTracker.map((tracker, index) => (
                <div key={index}

                    id={"Tracker" + index}
                >
                    <div className="inlineBlock">
                        <input type="text"
                            className={this.decideColor(tracker) + " form-control"}
                            value={tracker.initiative + " " + tracker.name}
                        ></input>
                    </div>

                    <button
                        className="btn btn-primary"
                        onClick={() => this.setDisabled(index)}
                        style={endTurnStyle}
                    > End Turn</button>
                </div>
            ))}
            <p className="bolded">Current Round: {this.state.round}
            </p>
            <button
                className="btn btn-primary"
                onClick={() => this.newRound()}
            >New Round</button>
        </div>
    }

    newRound() {
        this.state.initiativeTracker.forEach(element => {
            element.isYellow = true;
            element.isGreen = false;
            element.isDisabled = false;
        });
        this.setState({
            round: this.state.round + 1,
            initiativeTracker: this.state.initiativeTracker
        })

        this.setNextElement(-1);
    }

    setDisabled(index) {

        this.state.initiativeTracker[index].isYellow = false;
        this.state.initiativeTracker[index].isGreen = false;
        this.state.initiativeTracker[index].isDisabled = true;
        this.setState({ initiativeTracker: this.state.initiativeTracker })
        this.setNextElement(index)
    }

    setNextElement(index) {
        const newGreenAndYellowStateForNextElement = this.state.initiativeTracker.map((tracker, sidx) => {
            if (index + 1 !== sidx) return tracker;
            return { ...tracker, isGreen: true, isDisabled: false, isYellow: false }
        })



        this.setState({ initiativeTracker: newGreenAndYellowStateForNextElement })
    }

    public render() {

        return <div>
            <h1>Basic Initiative Tracker</h1>
            <form onSubmit={e => this.handleSubmit(e)}>

                {this.state.initiativeTracker.map((monsters, index) => (
                    <div key={index}>
                        <InitiativeTrackerInput
                            name={FieldNames.MonsterName}
                            placeholder={FieldNames.MonsterName}
                            label={FieldNames.MonsterName}
                            value1={monsters.initiative}
                            onChange1={e => this.handleChangeInitiative(e, index)}
                            value2={monsters.name}
                            onChange2={e => this.handleChangeName(e, index)}
                            count={index}
                            onClick={() => this.handleRemove(index)}

                        />
                    </div>
                ))}

                <button
                    className="btn btn-primary"
                    style={{ marginTop: '0.5em' }}
                    title="Add More"
                    onClick={() => this.handleAdd()}
                > + </button>
                <br></br>

                <button
                    className="btn btn-primary"
                    style={{ marginTop: '0.5em' }}
                    type="submit"
                    title="submit"
                    onClick={() => this.setState({ showInitiative: true })}
                > submit </button>
                {this.state.showInitiative === true ? this.show() : null}

            </form>
        </div>
    }


}

interface IInitiativeTracker {
    count: number;
    round: number;
    showInitiative: boolean;
    monsterName: string;
    initiativeTracker: any;

}
