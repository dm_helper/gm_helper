import * as React from 'react';



const initiativeStyle ={
    marginRight: '0.5em',
    width: '50%'
}

export default class InitiativeTrackerInput extends React.Component<{name,label,placeholder,value1, value2,onChange1, count, onClick, onChange2},{}>{
    constructor (props: any){
        super (props);       
    }
    
    

    public render(): JSX.Element {
        return <div>

            <div className="inlineBlock">
                <label>Initiative</label>
                <div>
                    <input
                        style={initiativeStyle}
                        className="form-control"
                        type="text"
                        name="Initiative"
                        placeholder="Initiative"
                        value={this.props.value1}
                        onChange={this.props.onChange1}
                    >
                    </input>
                </div>
            </div>

            <div className="inlineBlock">
                <label>Name</label>
                <div >
                    <input type="text"
                        className="form-control"
                        name="Name"
                        placeholder="Name"
                        value={this.props.value2}
                        onChange={this.props.onChange2}
                        
                    ></input>
                </div>
            </div>

            <button
                    className="btn btn-primary"
                    style={{ marginLeft: '0.5em' }}
                    title="Add More"
                    onClick={this.props.onClick}
                > - </button>

        </div>
    }
}
