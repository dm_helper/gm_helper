import * as React from 'react';

export default class TextInput extends React.Component<{name,label,placeholder,value,onChange},{}>{
    constructor (props: any){
        super (props);       
    }
    
    public render(): JSX.Element {
        return <div>
            <label htmlFor={this.props.name}>{this.props.label}</label>            
            <div>
                <input type="text"
                    className="form-control"
                    name={this.props.name}
                    placeholder={this.props.placeholder}
                    value={this.props.value}
                    onChange={this.props.onChange}
                >
                </input>
            </div>
        </div>
    }
}
