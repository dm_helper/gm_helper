import * as React from 'react';


export default class DisplaySpellData extends React.Component<spellDetails, any> {

    constructor (props:any) {
        super(props)              
    }
  

    private _spellLevelDecider(spellLvl: string)
    {
        var spellLevel = parseInt(spellLvl)
        if (spellLevel === 0) {
            return <span className="bolded italic">Cantrip </span>;
        }
        else if (spellLevel === 1) {
            return <span className="bolded italic">{spellLevel}st-level </span>;
        }
        else if (spellLevel === 2) {
            return <span className="bolded italic">{spellLevel}nd-level </span>;
        }
        else if (spellLevel === 3) {
            return <span className="bolded italic">{spellLevel}rd-level </span>;
        }
        else{
            return <span className="bolded italic">{spellLevel}th-level </span>;
        }       
    }
    public render() {
        const spell = this.props;
            return <div>
           
            <h2>{spell.name}</h2>
            <div>
                {this._spellLevelDecider(spell.level)}
                <span className="bolded italic">{spell.school} (spell school) </span> <br></br>
                
                <span className="bolded">Casting Time: </span>
                <span>{spell.castingTime} </span> <br></br>

                <span className="bolded">Range: </span>
                <span>{spell.range} </span> <br></br>

                <span className="bolded">Components: </span>
                <span>{spell.components} </span> <br></br>

                <span className="bolded">Concentration: </span>
                <span>{spell.concentration} </span> <br></br>

                <span className="bolded">Duration: </span>
                <span>{spell.duration} </span> <br></br>
                
                <span className="bolded">Castable by: </span>
                {spell.classes != null ? (spell.classes.map(singleClass=>(
                <span key={singleClass}> {singleClass}s</span>
            ))):null}
            </div>
            {/* <span>{spell.url} </span> */}            
            {/* <span>{spell.source} </span> */}
            <div className="spellDescription">
                <span >{spell.description} </span>
            </div>          
            
                                           
        </div>      
        
    }
    
    
    
}
interface spellDetails{
    id: number;
    name: string;
    url: string;
    level: string;
    school: string;
    castingTime: string;
    ritual: string;
    concentration: string;
    source: string;
    components: string;
    duration: string;
    range: string;
    description: string;
    classes: Array<string>;
}