import * as React from 'react';
import { RouteComponentProps, Link } from 'react-router-dom';
import { fetch, addTask } from 'domain-task';
import { render } from 'react-dom';

export default class ShowMonsterStats extends React.Component<{state, name,size, type:string, alignment, id}, any> {

    constructor (props:any) {
        super(props)              
    }

    deleteMonster(nameM){
        return fetch(`api/Monster/DeleteMonster?name=`+nameM,{
            method: 'delete'
        })        
                .then(response => response.text());   
    }
  
    public render() {
        const monster = this.props;
        if (monster.state) {
            return <div>
           
                        
            <span><Link to={'/UpdateMonster/' + monster.id}>{monster.name} </Link></span>
            <span>{monster.size} </span>
            <span>{monster.type} </span>
            <span>{monster.alignment} </span>
            <span>{monster.id} </span>
            <button onClick={()=>this.deleteMonster(monster.name)}>Delete</button>
           
           
                                
        </div>
        }
        else{
            return <div>
               Something
        </div>
        }
        
    }
    
    
    
}