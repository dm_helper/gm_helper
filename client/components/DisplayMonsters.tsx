import * as React from 'react';
import { RouteComponentProps, Link } from 'react-router-dom';
import { fetch, addTask } from 'domain-task';
import ShowMonsterStats from './DisplayData/ShowMonsterStats';



export default class DispplayMonsters extends React.Component<RouteComponentProps<{}>, IMonster> {

    constructor (props:any) {
        super(props)

        this.state ={
            currentName:"Select Monster",
            something: false,
            lol:[],
            monsters:[],
            sendMonster:{
                id:0,
                name:"",
                size:"",
                type:"",             
                alignment:"",
            }
        };
        this.handleChange = this.handleChange.bind(this);
    }
    componentDidMount()
    {                 
                fetch(`api/Monster/GetMonsters`)
                .then(response => response.json())
                .then(data => this.setState({ lol: data })
                );         
    }

    async getMonster(nameM){
        await fetch(`api/Monster/GetMonster?name=`+nameM)        
                .then(response => response.json())
                .then(monster => this.setState({ monsters: monster }));    
                
                
          this.setProperStates()      
    }
  
    setProperStates(){
        this.setState({
            sendMonster:{
                ...this.state.sendMonster,
                name: this.state.monsters.monsterNaming.name,
                type: this.state.monsters.monsterNaming.type,
                size:this.state.monsters.monsterNaming.size,
                alignment:this.state.monsters.monsterNaming.alignment,
                id: this.state.monsters.id
            }
        })
                
    }

    handleChange(event) {
        this.setState({currentName: event.target.value});
      }

    public render() {
        const {lol} = this.state;
        const monster= this.state.sendMonster;
        return <div>
            <div>
            <select
            value={this.state.currentName}
            onChange={this.handleChange}>

            <option>Select Monster</option>
            {lol.map(monster=>(
               <option key ={monster.id} value={monster.monsterNaming.name}>{monster.monsterNaming.name}</option>
               
           ))}
            </select>

            <button onClick={()=>
            {this.getMonster(this.state.currentName);
            this.setState({something: true})}}>
            Find</button>
            
            <ShowMonsterStats
                state={this.state.something}
                name={monster.name}
                size={monster.size}
                type={monster.type}
                alignment={monster.alignment}
                id={monster.id}
             />
           
            </div>           
           
        </div>
    }
    

    
}


 interface IMonster{

    currentName:string;
    something:boolean;
    monsters: any;
    lol:any;
    sendMonster: IMonsterStats;
 }
 
interface IMonsterStats{
    id: number;
    name: string;
    size: string;
    type: string;
    alignment: string;
}


