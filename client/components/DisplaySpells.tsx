import * as React from 'react';
import { RouteComponentProps, Link } from 'react-router-dom';
import { fetch, addTask } from 'domain-task';
import DisplaySpellData from './DisplayData/DisplaySpellData';
import Select from 'react-select';
import * as SpellComponentDetail from './hardCodedText/spellComponents'
import * as spellSchoolDescriptions from './hardCodedText/spellSchoolDescriptions'
import { AbjurationDetails, Conjuration, ConjurationDetails, DivinationDetails, EnchantmentDetails, EvocationDetails, IllusionDetails, NecromancyDetails, TransmutationDetails } from './hardCodedText/spellSchoolDescriptions';


export default class DispplaySpells extends React.Component<RouteComponentProps<{}>, IMonster> {

    constructor (props:any) {
        super(props)

        this.state ={
            currentName:"Select Monster",
            something: false,
            showSpellComononents:false,
            showSchoolDescriptions:false,
            allSpells:[],
            singleSpell:[],
            sendMonster:{
                id:0,
                name:"",
                size:"",
                type:"",             
                alignment:"",
            }
        };
        this.handleChange = this.handleChange.bind(this);
    }
    async componentDidMount()
    {                 
                await fetch(`api/Spells/GetSpells`)
                .then(response => response.json())
                .then(data => this.setState({ allSpells: data })
                );    
                            
    }

    async getSpell(nameM){
        await fetch(`api/Spells/GetSpell?name=`+nameM)        
                .then(response => response.json())
                .then(spell => this.setState({ singleSpell: spell }));    
                 
                this.setState({something: true})
    }

    addSpells() {
        fetch('https://localhost:5001/api/Spells/AddSpell?', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "Name": "Scatter",
                "Url": "https://www.dnd-spells.com/spell/scatter",
                "Level": "6",
                "School": "Conjuration",
                "CastingTime": "1 Action",
                "Ritual": "No",
                "Concentration": "No",
                "Source": "Xanathars Guide To Everything",
                "Components": "V",
                "Duration": "Instantaneous",
                "Range": "30 feet",
                "Description": "The air quivers around up to five creatures of your choice that you can see within range. An unwilling creature must succeed on a Wisdom saving throw to resist this spell. You teleport each affected target to an unoccupied space that you can see within 120 feet of you. That space must be on the ground or on a floor.\n",
                "Classes": [
                    "Sorcerer",
                    "Warlock",
                    "Wizard"
                ]

            })
        })
    }
    
    DeleteAllSpells(){
        return fetch(`api/Spells/DeleteAllSpells`,{
            method: 'delete'
        })        
                .then(response => response.text());   
    }

    ReadAllSpells(){
        return fetch(`api/Spells/ReadAllSpells`,{
            method: 'delete'
        })        
                .then(response => response.text());   
    }

    ShowComponentMeanings(){
        return<div className="spellDescription">
            <ul>
                <li className="bolded">{SpellComponentDetail.V}</li>
                <p>{SpellComponentDetail.Vdetail}</p>
                <li className="bolded">{SpellComponentDetail.S}</li>
                <p>{SpellComponentDetail.Sdetail}</p>
                <li className="bolded">{SpellComponentDetail.M}</li>
                <p>{SpellComponentDetail.Mdetail}</p>
            </ul>
        </div>
    }

    ShowSpellSchoolDescriptions() {
        return <div>
            <div className="spellDescription">
                <span className="bolded">{spellSchoolDescriptions.Abjuration} </span>
                <span>{spellSchoolDescriptions.AbjurationDetails}</span> <br></br>
            </div>

            <div className="spellDescription">
                <span className="bolded btnTop">{spellSchoolDescriptions.Conjuration} </span>
                <span>{spellSchoolDescriptions.ConjurationDetails}</span> <br></br>
            </div>

            <div className="spellDescription">
                <span className="bolded btnTop">{spellSchoolDescriptions.Divination} </span>
                <span>{spellSchoolDescriptions.DivinationDetails}</span> <br></br>
            </div>

            <div className="spellDescription">
                <span className="bolded btnTop">{spellSchoolDescriptions.Enchantment} </span>
                <span>{spellSchoolDescriptions.EnchantmentDetails}</span> <br></br>
            </div>

            <div className="spellDescription">
                <span className="bolded btnTop">{spellSchoolDescriptions.Evocation} </span>
                <span>{spellSchoolDescriptions.EvocationDetails}</span> <br></br>
            </div>

            <div className="spellDescription">
                <span className="bolded btnTop">{spellSchoolDescriptions.Illusion} </span>
                <span>{spellSchoolDescriptions.IllusionDetails}</span> <br></br>
            </div>

            <div className="spellDescription">
                <span className="bolded btnTop">{spellSchoolDescriptions.Necromancy} </span>
                <span>{spellSchoolDescriptions.NecromancyDetails}</span> <br></br>
            </div>

            <div className="spellDescription">
                <span className="bolded btnTop">{spellSchoolDescriptions.Transmutation} </span>
                <span>{spellSchoolDescriptions.TransmutationDetails}</span> <br></br>
            </div>

        </div>
    }

    async handleChange(event) {
        await this.setState({currentName: event.value});
        const currentName = this.state.currentName
        if(currentName!== undefined && currentName !== null && currentName !== ""){
            this.getSpell(this.state.currentName);
        }
              
      }

    public render() {
        const {allSpells} = this.state;
        const {singleSpell}= this.state;
        return <div>
            <h1>Spells</h1>
            
            <Select 
            onChange={this.handleChange}
            placeholder="Search and Select Spell"
            instanceId = {2}
            options ={allSpells.map(spell=>(
                {label: spell.name, value: spell.name}
            
            ))}/>


            {this.state.something?
             <DisplaySpellData
             id={singleSpell.id}
             name = {singleSpell.name}
             url= {singleSpell.url}
             level= {singleSpell.level}
             school= {singleSpell.school}
             castingTime= {singleSpell.castingTime}
             ritual= {singleSpell.ritual}
             concentration= {singleSpell.concentration}
             source= {singleSpell.source}
             components= {singleSpell.components}
             duration= {singleSpell.duration}
             range= {singleSpell.range}
             description= {singleSpell.description}
             classes= {singleSpell.classes}
             /> : null }

             
             <div className="btnTop">  
             <button className="btn btn-primary" onClick={()=>this.setState({showSpellComononents:!this.state.showSpellComononents})}>Show Component Meanings</button>
             {this.state.showSpellComononents ===true ? this.ShowComponentMeanings(): null}
                 </div>

            <div className="btnTop">  
             <button className="btn btn-primary" onClick={()=>this.setState({showSchoolDescriptions:!this.state.showSchoolDescriptions})}>Show Spell School Descriptions</button>
             {this.state.showSchoolDescriptions ===true ? this.ShowSpellSchoolDescriptions(): null}
                 </div>
            {/* <button onClick={()=>this.addSpells() }>
            addSPells</button> */}
           
            {/* <button onClick={()=>this.DeleteAllSpells() }>
            deleteAll</button>

            <button onClick={()=> this.ReadAllSpells()}> AddALll</button> */}                   
           
        </div>
    }
    

    
}


 interface IMonster{

    currentName:string;
    something:boolean;
    showSpellComononents:boolean;
    showSchoolDescriptions:boolean;
    singleSpell: any;
    allSpells:any;
    sendMonster: IMonsterStats;
 }
 
interface IMonsterStats{
    id: number;
    name: string;
    size: string;
    type: string;
    alignment: string;
}

interface ISpellData{
    Name: string,
    Url: string,
    Level: string,
    School: string,
    CastingTime: string,
    Ritual: string,
    Concentration: string,
    Source: string,
    Components: string,
    Duration: string,
    Range: string,
    Description: string,
    Classes: Array<string>
}
