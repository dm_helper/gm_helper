import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import TextInput from './Common/TextInput';
import * as FieldNames from './Common/FieldNameLablePlaceHolder';
import { MonsterSize, MonsterType, MonsterAligment } from './Common/FieldNameLablePlaceHolder';


export default class MonsterForm extends React.Component<RouteComponentProps<{}>, IMonster> {
    constructor(props: any) {
        super(props)

        this.state = {
            monsterName: "",
            monsterSize: "",
            monsterType: "",
            monsterAlignment:"",

            monsters: 
                {
                    name: "Holder",
                    size: "Holder",
                    type: "Holder",
                    alignment: "Holder"
                }
            
        };
    }

    private handleSubmit(e: React.FormEvent<HTMLFormElement>): void {
        e.preventDefault();
        const setDetails = this.state;

        var newMonster: IMonsterStats = {
            name: setDetails.monsterName,
            size: setDetails.monsterSize,
            type: setDetails.monsterType,
            alignment: setDetails.monsterAlignment,
        }
        this.AddMonster(newMonster);
    }

    private AddMonster(monster: IMonsterStats) {

        fetch('https://localhost:5001/api/Monster/SetMonster?', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "monsterNaming": {
                    "name": monster.name,
                    "size": monster.size,
                    "type": monster.type,
                    "alignment": monster.alignment,
                }

            })
        })
    }
    public render() {
        return <div>
            <h1>Add Your Monster</h1>
            <form onSubmit={e => this.handleSubmit(e)}>

                <TextInput
                    name={FieldNames.MonsterName}
                    placeholder={FieldNames.MonsterName}
                    label={FieldNames.MonsterName}
                    value={this.state.monsterName}
                    onChange={e => this.setState({ monsterName: e.target.value })}
                />
                <TextInput
                    name={FieldNames.MonsterSize}
                    placeholder={FieldNames.MonsterSize}
                    label={FieldNames.MonsterSize}
                    value={this.state.monsterSize}
                    onChange={e => this.setState({ monsterSize: e.target.value })}
                />

                <TextInput
                    name={FieldNames.MonsterType}
                    placeholder={FieldNames.MonsterType}
                    label={FieldNames.MonsterType}
                    value={this.state.monsterType}
                    onChange={e => this.setState({ monsterType: e.target.value })}
                />

                <TextInput
                    name={FieldNames.MonsterAligment}
                    placeholder={FieldNames.MonsterAligment}
                    label={FieldNames.MonsterAligment}
                    value={this.state.monsterAlignment}
                    onChange={e => this.setState({ monsterAlignment: e.target.value })}
                />


                <div className="btnTop">
                    <button
                        type="submit"
                        className="btn btn-primary"
                    > Save</button>
                </div>
            </form>
        </div>
    }


}

interface IMonster {
    monsterName: string;
    monsterSize: string;
    monsterType: string;
    monsterAlignment: string;
    monsters: IMonsterStats;

}

interface IMonsterStats {
    name: string;
    size: string;
    type: string;
    alignment: string;

}