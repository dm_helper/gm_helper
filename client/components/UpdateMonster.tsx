import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import TextInput from './Common/TextInput';
import * as FieldNames from './Common/FieldNameLablePlaceHolder';


export default class UpdateMonster extends React.Component<RouteComponentProps<{}>, IMonster> {
    constructor (props:any) {
        super(props)

        this.state = {
            monsterName: "",
            monsterSize: "",
            monsterType: "",
            monsterAlignment:"",

            monsters: 
                {
                    id: 0,
                    name: "Holder",
                    size: "Holder",
                    type: "Holder",
                    alignment: "Holder"
                }
            
        };
    }


    async componentDidMount()
    {           
        var monsterID = window.location.href.replace( /^\D+/g, '').replace('5001','').replace(/^\D+/g, '');
                           
            await fetch(`api/Monster/GetMonster?id=`+monsterID)        
            .then(response => response.json())
            .then(monster => this.setState({ monsters: monster }));
            
            this.updateInitialStates();
    }

    private updateInitialStates(){
        this.setState({
            monsterName: this.state.monsters.monsterNaming.name,
            monsterSize: this.state.monsters.monsterNaming.size,
            monsterType:  this.state.monsters.monsterNaming.type,
            monsterAlignment:  this.state.monsters.monsterNaming.alignment
        })
    }
    private handleSubmit(e: React.FormEvent<HTMLFormElement>): void {
        e.preventDefault();
        const state = this.state;

        var monstah:IMonsterStats ={
            id: state.monsters.id,
            name: state.monsterName,
            size: state.monsterSize,
            type: state.monsterType,
            alignment: state.monsterAlignment
        }

        this.UpdateMonster(monstah);       
    }

    private UpdateMonster(monster: IMonsterStats){      
        fetch('https://localhost:5001/api/Monster/UpdateMonster?', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "Id": monster.id,
                "monsterNaming":{
                    "Name": monster.name,
                    "size": monster.size,
                    "type":monster.type,
                    "alignment": monster.alignment
                }
               
            })
        })
    }
    

    // public renderMonsters(): JSX.Element[] {
    //     return this.state.monsters.map((monster: IMonsterStats)=>{
    //         return(
    //             <div key ={monster.id}>
    //             <span>{monster.id} </span>
    //             <span>{monster.nameM} </span>
    //             <span>{monster.race} </span>
    //             </div>
    //         )
    //     });

    // }

    public render() {
        return <div>
            <h1>Add Your Monster</h1>
            <form onSubmit={e=>this.handleSubmit(e)}>              
            <TextInput
                    name={FieldNames.MonsterName}
                    placeholder={FieldNames.MonsterName}
                    label={FieldNames.MonsterName}
                    value={this.state.monsterName}
                    onChange={e => this.setState({ monsterName: e.target.value })}
                />
                <TextInput
                    name={FieldNames.MonsterSize}
                    placeholder={FieldNames.MonsterSize}
                    label={FieldNames.MonsterSize}
                    value={this.state.monsterSize}
                    onChange={e => this.setState({ monsterSize: e.target.value })}
                />

                <TextInput
                    name={FieldNames.MonsterType}
                    placeholder={FieldNames.MonsterType}
                    label={FieldNames.MonsterType}
                    value={this.state.monsterType}
                    onChange={e => this.setState({ monsterType: e.target.value })}
                />

                <TextInput
                    name={FieldNames.MonsterAligment}
                    placeholder={FieldNames.MonsterAligment}
                    label={FieldNames.MonsterAligment}
                    value={this.state.monsterAlignment}
                    onChange={e => this.setState({ monsterAlignment: e.target.value })}
                />
                
                
                <div>
                <button 
                type="submit"
                className="btn btn-primary"
                > Update</button>
                </div>               
            </form>

        </div>
    }
    
    
}

interface IMonster {
    monsterName: string;
    monsterSize: string;
    monsterType: string;
    monsterAlignment: string;
    //monsters: IMonsterStats;
    monsters: any;

}

interface IMonsterStats {
    id: number;
    name: string;
    size: string;
    type: string;
    alignment: string;

}