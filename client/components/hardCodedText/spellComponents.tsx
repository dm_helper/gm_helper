export const V = "Verbal (V)";
export const S = "Somatic (S)";
export const M = "Material (M)";
export const F = "Focus (F)";
export const DF = "Divine Focus (DF)";
export const XP = "XP Cost (XP)";


export const Vdetail = "A verbal component is a spoken incantation. To provide a verbal component, you must be able to speak in a strong voice. A silence spell or a gag spoils the incantation (and thus the spell). A spellcaster who has been deafened has a 20% chance to spoil any spell with a verbal component that he or she tries to cast.";
export const Sdetail = "A somatic component is a measured and precise movement of the hand. You must have at least one hand free to provide a somatic component."
export const Mdetail = "A material component is one or more physical substances or objects that are annihilated by the spell energies in the casting process. Unless a cost is given for a material component, the cost is negligible. Don’t bother to keep track of material components with negligible cost. Assume you have all you need as long as you have your spell component pouch."
export const Fdetail = "A focus component is a prop of some sort. Unlike a material component, a focus is not consumed when the spell is cast and can be reused. As with material components, the cost for a focus is negligible unless a price is given. Assume that focus components of negligible cost are in your spell component pouch"
export const DFdetail = "A divine focus component is an item of spiritual significance. The divine focus for a cleric or a paladin is a holy symbol appropriate to the character’s faith"
export const XPdetail = "Some powerful spells entail an experience point cost to you. No spell can restore the XP lost in this manner. You cannot spend so much XP that you lose a level, so you cannot cast the spell unless you have enough XP to spare. However, you may, on gaining enough XP to attain a new level, use those XP for casting a spell rather than keeping them and advancing a level. The XP are treated just like a material component—expended when you cast the spell, whether or not the casting succeeds."