import * as React from 'react';
import { Route } from 'react-router-dom';
import { Layout } from './components/Layout';
import Home from './components/Home';
import FetchData from './components/FetchData';
import Counter from './components/Counter';
import MonsterForm from './components/MonterForm';
import DispplayMonsters from './components/DisplayMonsters';
import UpdateMonster from './components/UpdateMonster';
import DispplaySpells from './components/DisplaySpells';
import BasicInitiativeTracker from './components/BasicInitiativeTracker';

export const routes = <Layout>
    <Route exact path='/' component={ Home } />
    <Route path='/counter' component={ Counter } />
    <Route path='/fetchdata/:startDateIndex?' component={ FetchData } />
    <Route path='/MonsterForm' component={ MonsterForm } />
    <Route path='/UpdateMonster/:id' component={ UpdateMonster } />
    <Route path='/DisplayMonsters' component={ DispplayMonsters } />
    <Route path='/DisplaySpells' component={ DispplaySpells } />
    <Route path='/BasicInitiativeTracker' component={ BasicInitiativeTracker } />
</Layout>;
