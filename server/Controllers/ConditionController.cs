using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using server.Models.Conditions;
using LiteDB;

namespace server.Controllers
{
    [Route("api/[controller]")]
    public class ConditionController : Controller
    {
        private static LiteRepository Conditions = new LiteRepository("Condition.db");


        [HttpGet("[action]")]
        public Conditions GetCondtion(string name, int id)
        {
            try
            {
                return Conditions.First<Conditions>(x => x.Id == id);
            }
            catch (Exception)
            {
                
                return Conditions.First<Conditions>(x => x.Name == name);
            }
            
        }
     

        [HttpGet("[action]")]
        public List<Conditions> GetCondtions()
        {
            return Conditions.Query<Conditions>().ToList();
        }

        [HttpPost("[action]")]
        public ActionResult AddCondition(Conditions condition)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Conditions.Insert(condition);
            return Ok();
        }

    }
}
