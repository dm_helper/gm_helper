using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using server.Models.Monster;
using LiteDB;

namespace server.Controllers
{
    [Route("api/[controller]")]
    public class MonsterController : Controller
    {
        private static LiteRepository Monsters = new LiteRepository("Monsters.db");


        [HttpGet("[action]")]
        public Monster GetMonster(string name, int id)
        {
            try
            {
                return Monsters.First<Monster>(x => x.Id == id);
            }
            catch (Exception)
            {
                
                return Monsters.First<Monster>(x => x.MonsterNaming.Name == name);
            }
            
        }

       

        [HttpGet("[action]")]
        public List<Monster> GetMonsters()
        {
            return Monsters.Query<Monster>().ToList();
        }



        [HttpPost("[action]")]
        public ActionResult SetMonster([FromBody] Monster monster)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Monsters.Insert(monster);
            return Ok();
        }

        [HttpPost("[action]")]
        public void UpdateMonster([FromBody] Monster monster)
        {
            Monsters.Update<Monster>(monster);
        }

        [HttpDelete("[action]")]
        public void DeleteMonster(string name, int id)
        {
            if (string.IsNullOrEmpty(name) || string.IsNullOrWhiteSpace(name))
            {
                Monsters.Delete<Monster>(Monsters.First<Monster>(x => x.Id == id).Id);
            }
            else
            {
                Monsters.Delete<Monster>(Monsters.First<Monster>(x => x.MonsterNaming.Name == name).Id);
            }    
        }

        // [HttpDelete("[action]")]
        // public void DeleteMonster(string name)
        // {          
        //     Monsters.Delete<Monster>(Monsters.First<Monster>(x => x.MonsterNaming.Name == name).Id);
        // }
    }
}
