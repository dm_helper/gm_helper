using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using server.Models.Spells;
using LiteDB;
using System.IO;
using Newtonsoft.Json;

namespace server.Controllers
{
    [Route("api/[controller]")]
    public class SpellsController : Controller
    {
        private static LiteRepository Spells = new LiteRepository("Spells.db");


        [HttpGet("[action]")]
        public Spell GetSpell(string name, int id)
        {
            try
            {
                return Spells.First<Spell>(x => x.Id == id);
            }
            catch (Exception)
            {
                
                return Spells.First<Spell>(x => x.Name == name);
            }
            
        }

       

        [HttpGet("[action]")]
        public List<Spell> GetSpells()
        {
            return Spells.Query<Spell>().ToList();
        }



        [HttpPost("[action]")]
        public ActionResult AddSpell([FromBody] Spell Spell)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Spells.Insert(Spell);
            return Ok();
        }

        [HttpPost("[action]")]
        public void UpdateSpell([FromBody] Spell Spell)
        {
            Spells.Update<Spell>(Spell);
        }

        [HttpDelete("[action]")]
        public void DeleteSpell(string name, int id)
        {
            if (string.IsNullOrEmpty(name) || string.IsNullOrWhiteSpace(name))
            {
                Spells.Delete<Spell>(Spells.First<Spell>(x => x.Id == id).Id);
            }
            else
            {
                Spells.Delete<Spell>(Spells.First<Spell>(x => x.Name == name).Id);
            }    
        }


         [HttpDelete("[action]")]
        public void DeleteAllSpells()
        {
            for (int i = 0; i <= Spells.Query<Spell>().Count(); i++)
            {
                Spells.Delete<Spell>(Spells.First<Spell>(x => x.Id > 0).Id);
            }
                
            
                
        }

        [HttpDelete("[action]")]
        public void ReadAllSpells()
        {
           List<Spell> spellsFull = ReadFromJsonFile<List<Spell>>("E:\\TryOther\\StartetKit\\gmHelper\\gm_helper\\client\\components\\Common\\spells.json");
                foreach (var spell in spellsFull)
                {
                    AddSpell(spell);
                }
            
                
        }
        public static T ReadFromJsonFile<T>(string filePath) where T : new()
        {
            TextReader reader = null;
            try
            {
                reader = new StreamReader(filePath);
                var fileContents = reader.ReadToEnd();
                return JsonConvert.DeserializeObject<T>(fileContents);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }
    }
}
