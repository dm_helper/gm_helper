using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace server.Models.Conditions
{
    public class Conditions
    {
        public int Id { get; set; }
        public string Name{get;set;}
        public List<string> Effect{get;set;}
       
    }
}
