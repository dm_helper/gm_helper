
using System.ComponentModel.DataAnnotations;

namespace server.Models.Monster
{
    public class MonsterNaming
    {
        [Required]
        public string Name {get;set;}
        public string Size{get; set;}
        public string Type{get; set;}
        public string Alignment{get; set;}
    }
}