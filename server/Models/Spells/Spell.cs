using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace server.Models.Spells
{
    public class Spell
    {
        public int Id { get; set; }
        
        public string Name{get;set;}
        public string Url{get;set;}
        public string Level{get;set;}
        public string School{get;set;}
        public string CastingTime{get;set;}
        public string Ritual{get;set;}
        public string Concentration{get;set;}
        public string Source{get;set;}
        public string Components{get;set;}
        public string Duration{get;set;}
        public string Range{get;set;}
        public string Description{get;set;}
        public List<string> Classes{get;set;}
       
    }
}
